# jenkins-pipeline-deploy-to-eks
# Project Name: End to end deployment of Applications to Kubernetes using a Jenkins CI/CD Pipeline
# Steps for the project

## 1. Create a Keypair that matches your keypair
## 2. Create a Jenkins Server with all the dependencies, libraries and packagies needed.
## 2. Once completed, access the Jenkins server and Set it up
## 4. Run the jenkins-pipeline-deploy-to-eks to create Kubernetes Cluster, create deployments and Services
## 5. Test that the application is running 
## 6. Destroy infrastructure
![image](/uploads/4cc8d22c0c4e5d2af9f3df66e52a7570/image.png)

![image](/uploads/1432711be44be2a514f2c6055bde1cac/image.png)

![image](/uploads/09484fd626af0d5c00853b92b2d1e551/image.png)

![image](/uploads/e2f0bd996fe647f05afa18d0fbce386a/image.png)

![image](/uploads/767896ea18da72fc9531ebe17213c105/image.png)

![image](/uploads/2a5c6dba3210c2046010ffd844482caf/image.png)

![image](/uploads/412096a46610ff39de1772c9706dd93c/image.png)

LB DNS URL:
![image](/uploads/dfeb06e8ba78f1bf331f96836faaede8/image.png)